# CKE Placeholder

CKE Placeholder provides a framework for enriching long text fields with widgets, media, tables and pretty much everything else in an easily maintainable manner that makes theming easy.

## The supplied sub modules are really only the most basic usage of the module. You can add widget types or library panes using the hooks specified in the api file.

## To get started
you can install the module and the CKE Placeholder File entity module. It creates tabs for documents and images from the media library and placeholders to insert it in the editor. You will most likely want to change the way images and documents are rendered or the way the list is generated.
